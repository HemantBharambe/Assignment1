import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {

        // ----------------- write your code BELOW this line only --------
        // your code here (add lines)
        Scanner myScanner = new Scanner(System.in);
        int numerIn = myScanner.nextInt();
        int denomIn = myScanner.nextInt();

        // Executing the Euclidean algorithm for GCD of the numerator and denominator for fraction reduction
        int numberA = numerIn;
        int numberB = denomIn;
        int remainder = numberA % numberB;

        while (remainder != 0) {

            numberA = numberB;
            numberB = remainder;
            remainder = numberA % numberB;
        }

        // numberB is the GCD, Reduce the result fraction
        int numerOut = numerIn / numberB;
        int denomOut = denomIn / numberB;

        numerIn = myScanner.nextInt();

        // Get inputs until -1, calculate the summery each loop
        while (numerIn != -1) {
            denomIn = myScanner.nextInt();

            // Calculate the numerator and denominator of the result summary
            numerOut = numerIn * denomOut + denomIn * numerOut;
            denomOut = denomIn * denomOut;

            // Executing the Euclidean algorithm for GCD of the numerator and denominator for fraction reduction
            numberA = numerOut;
            numberB = denomOut;
            remainder = numberA % numberB;

            while (remainder != 0) {

                numberA = numberB;
                numberB = remainder;
                remainder = numberA % numberB;
            }

            // numberB is the GCD, Reduce the result fraction
            numerOut = numerOut / numberB;
            denomOut = denomOut / numberB;

            numerIn = myScanner.nextInt();
        }

        System.out.println(numerOut);
        System.out.println(denomOut);
        // ----------------- write your code ABOVE this line only ---------

    }
}
